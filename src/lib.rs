
use pyo3::prelude::*;
use ring::digest;
use pyo3::types::PyBytes;

#[pyclass]
struct Sha512 {
    inner: Vec<u8>,
}


#[pymethods]
impl Sha512 {
    #[pyo3(name = "digest_size")]
    #[classattr]
    const DIGEST_SIZE: usize = digest::SHA512_OUTPUT_LEN;

    #[pyo3(name = "block_size")]
    #[classattr]
    const BLOCK_SIZE: usize = 1024 / 8;

    #[new]
    #[args(string = "Vec::new()")]
    fn new(string: Vec<u8>) -> Self {
        Self { inner: string }
    }

    pub fn update(&mut self, obj: &[u8]) {
        self.inner.extend_from_slice(obj)
    }

    // pub fn digest(&self, py: Python) -> PyObject {
    //     PyBytes::new(py, digest::digest(&digest::SHA512, &self.inner).as_ref()).into()
    // }

    pub fn digest(&self, py: Python) -> PyObject {
        PyBytes::new(py, &openssl::sha::sha512(&self.inner)).into()
    }

    pub fn copy(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

/// A Python module implemented in Rust.
#[pymodule]
fn rust_hashlib_testing(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Sha512>()?;
    Ok(())
}
