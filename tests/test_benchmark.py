import rust_hashlib_testing
import hashlib

def do_hash(hasher, data):
    h = hasher()
    h.update(data)
    return h.digest()

def test_rust(benchmark):
    benchmark(do_hash, rust_hashlib_testing.Sha512, b"123")

def test_python(benchmark):
    benchmark(do_hash, hashlib.sha512, b"123")
